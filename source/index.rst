.. Polynomial regression documentation master file, created by
   sphinx-quickstart on Sat Feb  3 20:30:03 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Polynomial regression's documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   auto_examples/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
