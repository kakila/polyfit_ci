"""
Polynomial fit confidence interval
==================================

Compute and plot confidence interval for a polynomial fit.
"""

import numpy as np
import matplotlib.pyplot as plt

# For reproducibility we seed the random generator
rng = np.random.default_rng(12345)

# %%
# Data generation
# ---------------
# We generate some artificial data to fit the polynomial
degree = 5
coefs_true = rng.random(degree + 1)

N = 15
x = np.linspace(-1, 1, 100)
xo = rng.choice(x, N)
Vo = np.vander(xo, degree + 1)
yo_true = Vo @ coefs_true
yo = yo_true + rng.normal(0, 0.1, size=xo.shape)

# %%
# Regression of the data
# ----------------------
# We regress the data assuming we know the correct degree.
#
# Using pilyfit
# *************
# The function :func:`numpy.polyfit` returns the estimated covariance of the polynomial coefficients.
# This covariance can be used to compute the variance of the predicted values.
# The first step is to realize that the evaluation of a polynomial is a scalar product
#
# .. math::
#    p(x) = v(x) \cdot c
#
# where :math:`c` is a column vector of polynomial coefficients (including the intercept), and
#
# .. math::
#    v(x) = \begin{bmatrix}x^d & x^{d-1} & \ldots & 1\end{bmatrix}
#
# is the polynomial feature vector of degree :math:`d`. For many, say :math:`N`, values of :math:`x` we can form the
# Vandermonde matrix, by stacking the polynomial feature vectors:
#
# .. math::
#    V(x) = \begin{bmatrix}v(x_1) \\ v(x_2) \\ \vdots \\ v(x_N)\end{bmatrix}
#
# The polynomial evaluated at all points is then
#
# .. math::
#    p(x) = V(x) c
#
# We will solve the regression problem
#
#  .. math::
#     y(x) = V(x) c + \epsilon
#
# That is, the observed data is a polynomial plus some noise. We assume that there are no uncertainties on the values
# of :math:`x`. Further we assume that the mean of the noise is zero, :math:`\bar{\epsilon} = 0`, and it has a fixed
# variance :math:`\sigma^2` without temporal correlations [#iid]_ [#cov]_:
#
# .. math::
#      \epsilon \sim \mathcal{N}(0, \Sigma_\epsilon) , \quad \Sigma_\epsilon = \sigma^2 \cdot I
#
# Let's now compute the expected value and the variance of the latent values (noiseless polynomial), imagining that the coefficients are distributed
#
# .. math::
#    \bar{p}(x) &= V(x) \bar{c}\\
#    \operatorname{Var} p(x) &= V(x) \left(c - \bar{c}\right)\left(c - \bar{c}\right)^\top V(x)^{\top} = V(x)\Sigma_c V(x)^{\top}
#
# where :math:`\Sigma_c` is the covariance of the polynomial coefficients. The variance of the latent values are
# the diagonal of :math:`\Sigma_c`. If we have the mean value (point estimate) and the covariance of the
# coefficients, then we can compute these two quantities.
#
# If we cared also about the total uncertainty of the generated data. That is, if we want to see the distribution of the
# values generated by the noisy model above; then the variance of those values has a contribution from the coefficients variance, and the noise itself:
#
# .. math::
#    \bar{y}(x) &= V(x) \bar{c}\\
#    \operatorname{Var} y(x) &= V(x)\Sigma_c V(x)^{\top} + \Sigma_\epsilon
#

coefs, cov = np.polyfit(xo, yo, degree, full=False, cov=True)


# %%
# We now compute variance of generated values.

def poly_variance(cov, x):
    """Compute variance of polynomial values using the covariance of the coefficients."""
    V = np.vander(x, cov.shape[0])
    return np.diag(V @ cov @ V.T)


yvar = poly_variance(cov, x)
y_ci = 1.96 * np.sqrt(yvar)
y_ = np.polyval(coefs, x)

fig, ax = plt.subplots()

ax.plot(x, np.polyval(coefs_true, x), label="latent", color="k")
ax.fill_between(x, y_ - y_ci, y_ + y_ci, label="CI", alpha=0.3)
ax.plot(x, y_, label="fitted")
ax.scatter(xo, yo, label="data")

plt.legend()


# %%
# Using Polynomial fit
# ********************
# The function :func:`numpy.polynomial.polynomial.Polynomial.fit` does not compute the covariance of the coefficients.
# We need to use the returned residuals to do this.
# The sum of the squared residuals (properly normalized) are our best estimate of the variance of the noise.
# First we recall the least squares point estimate of the coefficients:
#
# .. math::
#    y &= V c + \epsilon\\
#    V^\top y &=  V^\top V c + V^\top \epsilon\\
#    c_\epsilon \doteq \left(V^\top V\right)^{-1}V^\top y &= c + \left(V^\top V\right)^{-1} V^\top \epsilon
#
# The random variable :math:`c_\epsilon` is our estimation of the coefficients. This estimation is a random variable:
# it doesn't represent a single value of the coefficients, they are distributed. Since the noise is never exactly
# known, we need to get rid of it and obtain a point estimate. By averaging over noise realizations (zero mean) we get
#
# .. math::
#    E[c_\epsilon] = \left(V^\top V\right)^{-1}V^\top y = c
#
# Here I used :math:`E[\cdot]` to denote averaging over noise realizations. Then we use the formula for the covariance of the estimated coefficients
#
# .. math::
#    \Sigma_{c_\epsilon} \doteq E\left[c_\epsilon c_\epsilon^\top\right] - E[c_\epsilon]E\left[c_\epsilon^\top\right] = E\left[c_\epsilon c_\epsilon^\top\right] - c\,c^\top
#
# Replacing with our formula for the estimate we get
#
# .. math::
#    E\left[c_\epsilon c_\epsilon^\top\right] = E\left[\left(
#    \left(V^\top V\right)^{-1}V^\top y
#    \right)
#    \left(
#    \left(V^\top V\right)^{-1}V^\top y
#    \right)^\top\right]
#
# which can be further reduced using the right hand-side of the solution above:
#
# .. math::
#    E\left[c_\epsilon c_\epsilon^\top\right] = E\left[
#    \left(c + \left(V^\top V\right)^{-1}V^\top \epsilon\right)
#    \left(c + \left(V^\top V\right)^{-1}V^\top \epsilon\right)^\top\right] = \\
#    = E\left[cc^\top\right] + E\left[\left(V^\top V\right)^{-1}V^\top \epsilon \epsilon^\top V \left(V^\top V\right)^{-\top}  \right] = \\
#    = cc^\top +  \left(V^\top V\right)^{-1}V^\top E\left[\epsilon \epsilon^\top\right]  V \left(V^\top V\right)^{-\top} = \\
#    = cc^\top +  \left(V^\top V\right)^{-1}V^\top \Sigma_\epsilon  V \left(V^\top V\right)^{-\top} = \\
#    = cc^\top +  \sigma^2 \left(V^\top V\right)^{-\top}
#
# where the terms with :math:`\epsilon` are gone due to the zero mean property of the noise. In the last step we used
# the fact that the noise covariance is diagonal. The last transposition is also not needed since the matrix is
# symmetric. Finally:
#
# .. math::
#    \Sigma_{c_\epsilon} = \sigma^2 \left(V^\top V\right)^{-1}
#
# Which is an estimate of the covariance of the parameters needed to compute the covariance of the latent polynomial values.

def poly_coefs_cov(x, r2, degree):
    order = degree + 1
    V = np.vander(x, order)
    cov = np.linalg.inv(V.T @ V) * r2 / (len(x) - order)
    return cov


_, extra = np.polynomial.polynomial.Polynomial.fit(xo, yo, degree, full=True)
resid, rank, sv, rcond_ = extra
cov_ = poly_coefs_cov(xo, resid, degree)

yvar_ = poly_variance(cov_, x)
y_ci_ = 1.96 * np.sqrt(yvar_)
y__ = np.polyval(coefs, x)

fig, ax = plt.subplots()

ax.plot(x, np.polyval(coefs_true, x), label="latent", color="k")
ax.fill_between(x, y__ - y_ci_, y__ + y_ci_, label="CI", alpha=0.3)
ax.plot(x, y__, label="fitted")
ax.scatter(xo, yo, label="data")

plt.legend()

plt.show()

# %%
# .. rubric:: Footnotes
#
# .. [#iid] All these assumptions imply that the noise is independent and identically distributed (i.i.d.).
# .. [#cov] The assumption of uncorrelated noise can be relaxed. The formulas hold for any known covariance matrix of the noise.
